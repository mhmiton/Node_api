const express 		= require('express');
const mysql 		= require('mysql');
const bodyParser 	= require('body-parser');
const cors 			= require('cors');
const multer 		= require('multer');
const upload 		= multer({ dest : 'uploads/' });
const JWT 			= require('jsonwebtoken');
const JWT_SECRET 	= require('./config');

const app 			= express();
const port 			= 3200;

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(bodyParser());

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(cors());
app.options('*', cors());

app.listen(port, () => {
	console.log('Server is runing - http://localhost:'+port);
});

app.all('/*', upload.array(), function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type,accept,access_token,X-Requested-With');
    next();
});

apiToken = () => {
	return token = JWT.sign({
		sub:"apiAuthentication",
		name:"Mehediul Hassan Miton",
		iat:new Date().getTime()
	}, JWT_SECRET);
}

var con = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "12345",
	database: "node_api"
});

con.connect(function(err) {
  if (err) console.log(err);
  console.log("Connected mysql!");
});

app.get('/', (err, res) => {
	res.render('layouts/main', {content_path:'dashboard',token:apiToken()});
});

app.get('/student', (req, res) => {
	con.query("SELECT * FROM student ORDER BY id DESC", (err, results) => {
		if(err) console.log(err);
		res.render('layouts/main', {content_path:'student', data:results, token:apiToken()});
	});
});

//Test Token
app.post('/token', (req, res) => {
	if(req.body.token)
	{
		JWT.verify(req.body.token, JWT_SECRET, (err, decoded) => {
			if(err) res.json({err:err, text:'Your Token Is Invalid', type:'error', token:apiToken()});
			res.json({text:'Your Token Is Valid', type:'success', token:apiToken()});
		});
	} else {
		res.json({text:'Your Have No Token', type:'error', token:apiToken()});
	}
});

app.post('/data_list', (req, res) => {
	if(req.body.token)
	{
		JWT.verify(req.body.token, JWT_SECRET, (err, decoded) => {
			if(err) res.json({err:err, text:'Your Token Is Invalid', type:'error', token:apiToken()});
			con.query("SELECT * FROM student ORDER BY id DESC", (err, results) => {
				if (err) console.log(err);
				res.send(results);
			});
		});
	} else {
		res.json({text:'Your Have No Token', type:'error', token:apiToken()});
	}
});

app.post('/save', (req, res) => {
	if(req.body.token)
	{
		JWT.verify(req.body.token, JWT_SECRET, (err, decoded) => {
			if(err) res.json({err:err, text:'Your Token Is Invalid', type:'error', token:apiToken()});
			var data = {
				name:req.body.name,
				phone:req.body.phone,
				email:req.body.email,
				address:req.body.address,
			}

			if(req.body.id)
			{
				con.query("UPDATE student SET ? WHERE id = ?", [data,req.body.id], (err, results) => {
					if (err) console.log(err);
					res.json({text:'Your Data Update Successfull', type:'success'});
				});
			} else {
				con.query("INSERT INTO student set ?", data, (err, results) => {
					if (err) console.log(err);
					res.json({text:'Your Data Save Successfull', type:'success'});
				});
			}
		});
	} else {
		res.json({text:'Your Have No Token', type:'error', token:apiToken()});
	}
});

app.delete('/destroy', (req, res) => {
	if(req.body.token)
	{
		JWT.verify(req.body.token, JWT_SECRET, (err, decoded) => {
			if(err) res.json({err:err, text:'Your Token Is Invalid', type:'error', token:apiToken()});
			if(req.body.id)
			{
				con.query("DELETE FROM student WHERE id = ?",req.body.id,(err, results) => {
					if (err) console.log(err);
					res.json({text:'Your Data Delete Successfull', type:'success'});
				});
			} else {
				res.json({text:'Your ID Not Found !!!', type:'error'});
			}
		});
	} else {
		res.json({text:'Your Have No Token', type:'error', token:apiToken()});
	}
});

app.post('/edit_data', (req, res) => {
	if(req.body.token)
	{
		if(req.body.id)
		{
			JWT.verify(req.body.token, JWT_SECRET, (err, decoded) => {
				if(err) res.json({err:err, text:'Your Token Is Invalid', type:'error', token:apiToken()});
				con.query("SELECT * FROM student WHERE id = ?",req.body.id,(err, results) => {
					if (err) console.log(err);
					res.json(results);
				});
			});
		} else {
			res.json({text:'Your ID Not Found !!!', type:'error'});
		}
	} else {
		res.json({text:'Your Have No Token', type:'error', token:apiToken()});
	}
});
