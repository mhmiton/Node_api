-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 02, 2018 at 12:00 PM
-- Server version: 5.7.22-0ubuntu18.04.1
-- PHP Version: 7.2.5-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `node_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `phone`, `email`, `address`) VALUES
(2, 'Abdul Mia', '01787878788', 'abdul@gmail.com', 'Rampura'),
(4, 'Mehediul Hassan Miton', '01878988124', 'mdmiton321@gmail.com', 'Rampura, Dhaka - 1212'),
(5, 'Mehediul Hassan Miton', '01878988124', 'mdmiton321@gmail.com', 'Rampura, Dhaka - 1212'),
(6, 'Mehediul Hassan Miton', '01878988124', 'mdmiton321@gmail.com', 'Rampura, Dhaka - 1212'),
(7, 'Mehediul Hassan Miton', '01878988124', 'mdmiton321@gmail.com', 'Rampura, Dhaka - 1212'),
(9, 'Mehediul Hassan Miton', '01878988124', 'mdmiton321@gmail.com', 'Rampura, Dhaka - 1212'),
(11, 'Mehediul Hassan Miton', '01878988124', 'admin1@caretutors.com', 'Rampura, Dhaka - 1212'),
(12, 'Mehediul Hassan Miton', '01878988124', 'admin1@caretutors.com', 'Rampura, Dhaka - 1212'),
(13, 'Mehediul Hassan Miton', '01878988124', 'mdmiton321@gmail.com', 'Rampura, Dhaka - 1212'),
(14, 'Mehediul Hassan Miton', '01878988124', 'mdmiton321@gmail.com', 'Rampura, Dhaka - 1212'),
(22, 'Nakibul Hassan Choton', '015424242545', 'choton@gmail.com', 'Rampura, Dhaka - 1212'),
(23, 'Mehediul Hassan Miton', '01878988124', 'mdmiton321@gmail.com', 'Rampura, Dhaka - 1212'),
(24, NULL, NULL, NULL, NULL),
(25, NULL, 'fdsdfsfsfd', NULL, NULL),
(26, 'sdfsdfsdf', NULL, NULL, NULL),
(27, NULL, NULL, NULL, 'fsdfsdf'),
(28, NULL, NULL, 'sdfsdfsdf', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
